<?php

class Solution
{

    function romanToInt($s)
    {
        $base = [
            'I' => 1,
            'V' => 5,
            'X' => 10,
            'L' => 50,
            'C' => 100,
            'D' => 500,
            'M' => 1000,

            'IV' => 4,
            'XL' => 40,
            'CD' => 400,
            'IX' => 9,
            'XC' => 90,
            'CM' => 900
        ];
        $result = 0;
        $len = strlen($s);
        for ($i = 0; $i < $len; $i++) {
            $letter = $s[$i];
            $result += $base[$letter];
            if (isset($s[$i+1])) {
                $next = $s[$i+1];
                if (isset($base[$letter.$next])) {
                    $result += $base[$letter.$next];
                    $result -= $base[$letter];
                    $i++;
                }
            }
        }
        return $result;
    }
}

$sol = new Solution;


echo $sol->romanToInt('MCMXCIV') . PHP_EOL;
