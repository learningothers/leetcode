<?php

class Solution
{
    function threeSum($nums)
    {
        sort($nums);
        $len = count($nums);
        $result = [];
        $length = 0;
        for ($i = 0; $i < $len; $i++) {
            if ($nums[$i] > 0) {
                break;
            }
            if ($i > 0 && $nums[$i - 1] == $nums[$i]) {
                continue;
            }
            $point = (-1) * $nums[$i];
            $min = $i + 1;
            $max = $len - 1;

            while ($min < $max) {
                
                $sum = $nums[$min] + $nums[$max];
                
                if ($sum < $point) {
                    $min++;
                } else if ($sum > $point) {
                    $max--;
                } else {
                    $item = [$nums[$i], $nums[$min], $nums[$max]];
                    
                    $min++;
                    $max--;
                    if ($length > 0) {
                        $last = $result[$length - 1];
                        if ($last != $item) {
                            $result[] = $item;
                            $length++;
                        }
                    } else {
                        $result[] = $item;
                        $length++;
                    }
                }
            }
        }
        return $result;
    }
}

$data = [-4, -2, -2, -2, 0, 1, 2, 2, 2, 3, 3, 4, 4, 6, 6];
$data = require './data.php';

$time1 =  microtime(true);
echo (json_encode((new Solution())->threeSum($data))) . PHP_EOL;
$time = microtime(true);
echo "Milliseconds " . $time - $time1 . PHP_EOL;
