<?php

class Solution
{

    function generateParenthesis($n)
    {
        $data = ['()'];
        $data = ['()', ')('];
        $data = ['(())', '()()'];
        $data = ['())', '()()', '(())', '((()'];
        $data = ['(((())))', '((()()))', '((())())', '((()))()', '(()(()))', '(()()())', '(()())()', '(())(())'];
        $data = ["(((())))", "((()()))", "((())())", "((()))()", "(()(()))", "(()()())", "(()())()", "(())(())", "(())()()", "()((()))", "()(()())", "()(())()", "()()(())", "()()()()"];
        $data = ["(((())))", "()((()))", "()(()())", "()(())()", "(()(()))", "(()()())", "(()())()", "((()()))", "((())())", "((()))()"];

        $result = ['()'];
        $wrappers = [['(', ')'], ['', '()'], ['()', '']];
        for ($i = 2; $i <= $n; $i++) {
            $data = [];
            // $count = 0;
            $len = count($result);

            $wrappersCount = count($wrappers);
            for ($j = 0; $j < $wrappersCount; $j++) {
                [$left, $right] = $wrappers[$j];
                for ($k = 0; $k < $len; $k++) {
                    $item = $result[$k];

                    $wrap = ($left . $item . $right);

                    if (!in_array($wrap, $data)) {
                        $data[] = $wrap;
                    }
                    // if ($count == 0) {
                    //     $data[] = $wrap;
                    //     $count++;
                    //     continue;
                    // }
                    // if ($data[$count - 1] != $wrap) {
                    //     $data[] = $wrap;
                    //     $count++;
                    // }
                }
            }
            $result = $data;
        }
        return $result;
    }
}


echo json_encode((new Solution)->generateParenthesis(4)) . PHP_EOL;
