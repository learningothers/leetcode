<?php



function searchInsert($nums, $target)
{
    $min = 0;
    $max = count($nums) - 1;

    while ($min <= $max) {
        $mid = intdiv($min + $max, 2);
        if ($nums[$mid] < $target) {
            $min = $mid + 1;
        } else if ($nums[$mid] > $target) {
            $max = $mid - 1;
        } else {
            return $mid;
        }
    }
    return $min;
}


$nums = [1, 3, 5, 6, 88];
$target = 88;

echo searchInsert($nums, $target) . PHP_EOL;
