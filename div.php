<?php

class Solution
{

    function divide($dividend, $divisor)
    {
        $quotient = 0;
        $remainder = abs($dividend);
        $absDivisor = abs($divisor);

        // Find the highest bit that can be set
        while ($remainder >= $absDivisor) {
            $temp = $absDivisor;
            $multiple = 1;
            while ($remainder >= ($temp << 1)) {
                usleep(50000);
                echo "TEMP = $temp \n";
                $temp <<= 1;
                echo "TEMP = $temp \n";
                $multiple <<= 1;
            }
            $remainder -= $temp;
            $quotient += $multiple;
        }

        // Determine the sign of the result
        if (($dividend < 0 && $divisor > 0) || ($dividend > 0 && $divisor < 0)) {
            $quotient = -$quotient;
        }
        if ($quotient >= pow(2, 31)) {
            $quotient--;
        }

        return $quotient;
    }
}


$sol = new Solution;

// echo $sol->divide(-2147483648, -1) . PHP_EOL;
echo <<<EOH
 ---------     ---------  -- ---------
 result        value      op test
 ---------     ---------  -- ---------
EOH;

echo 1 << 10;
