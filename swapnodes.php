<?php


class ListNode
{
    public $val = 0;
    public ?ListNode $next = null;
    function __construct($val = 0, $next = null)
    {
        $this->val = $val;
        $this->next = $next;
    }
}

class Solution
{

    /**
     * @param ListNode $head
     * @return ListNode
     */
    function swapPairs($head)
    {
        if (! $head?->next) {
            return $head;
        } else {
            $next = $head->next;
            if ($th = $next->next) {
                $head->next = $this->swapPairs($th);
            } else {
                $head->next = null;
            }
            $next->next = $head;
            return $next;
        }
        return $head;
    }

    public function toListNode($array)
    {
        $len = count($array);
        $listNode = null;
        while ($len > 0) {
            $listNode = new ListNode(val: $array[$len - 1], next: $listNode);
            $len--;
        }
        return $listNode;
    }

    public function toArray($listNode)
    {
        if (!$listNode) return [];
        $array = [$listNode->val];
        while ($next = $listNode->next) {
            $array[] = $next->val;
            $listNode = $next;
        }
        return $array;
    }
}

$sol = new Solution;

$array = [1, 2, 3, 4, 5,6,7];

$l = $sol->toListNode($array);
$l = $sol->swapPairs($l);
$array = $sol->toArray($l);
echo json_encode($array) . PHP_EOL;
