<?php

class Solution
{
    function fourSum($nums, $target)
    {
        sort($nums);
        $len = count($nums);
        $count = 0;
        $result = [];
        for ($i = 0; $i < $len; $i++) {
            for ($j = $i + 1; $j < $len; $j++) {
                if ($nums[0] < 0 && $nums[$j] == $nums[$j - 1]) {
                    continue;
                }
                $point = $target - ($nums[$i] + $nums[$j]);
                $min = $j + 1;
                $max = $len - 1;
                while ($min < $max) {
                    $sum = $nums[$min] + $nums[$max];
                    if ($sum < $point) {
                        $min++;
                    } else if ($sum > $point) {
                        $max--;
                    } else {
                        $item = [$nums[$i] , $nums[$j], $nums[$min], $nums[$max]];
                        $min++;
                        $max--;
                        if ($count == 0) {
                            $result[] = $item;
                            $count++;
                        } else {
                            if ($result[$count-1] != $item) {
                                $result[] = $item;
                            }
                        }
                    }
                }
            }
        }
        return $result;
    }
}

$data = [-2,-1,-1,1,1,2,2];
$data = require './data.php';

$time1 =  microtime(true);
echo (json_encode((new Solution())->fourSum($data, 0))) . PHP_EOL;
$time = microtime(true);
// echo "Milliseconds " . $time - $time1 . PHP_EOL;
