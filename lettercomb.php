<?php


class Solution
{
    public $letters = [
        '1' => '',
        '2' => 'abc',
        '3' => 'def',
        '4' => 'ghi',
        '5' => 'jkl',
        '6' => 'mno',
        '7' => 'pqrs',
        '8' => 'tuv',
        '9' => 'wxyz'
    ];

    function letterCombinations($digits)
    {

        $len = strlen($digits);
        if ($len > 0) {
            return $this->single('', $digits, $len);
        } else {
            return [];
        }
    }

    public function single($letter, $digits, $len)
    {
        $data = [];
        if ($len > 0) {
            $letters = $this->letters[$digits[0]];
            $lenletters = strlen($letters);
            for ($i = 0; $i < $lenletters; $i++) {
                $res = $this->single($letter . $letters[$i], substr($digits, 1), ($len - 1));
                $data = [...$data, ...$res];
            }
        } else {
            $data[] = $letter;
        }
        return $data;
    }
}

$d = "2345";


echo json_encode((new Solution)->letterCombinations($d)) . PHP_EOL;
