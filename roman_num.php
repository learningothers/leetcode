<?php


class Solution
{


    function intToRoman($num)
    {
        $base = [
            1 => 'I',
            5 => 'V',
            10 => 'X',
            50 => 'L',
            100 => 'C',
            500 => 'D',
            1000 => 'M',

            4 => 'IV',
            40 => 'XL',
            400 => 'CD',
            9 => 'IX',
            90 => 'XC',
            900 => 'CM'
        ];

        $numString = (string)$num;
        $len = strlen($numString);
        $roman = '';
        for ($i = 0; $i < $len; $i++) {
            $result = (int)$numString[$i] * pow(10, $len - $i - 1);

            if (isset($base[$result])) {
                $roman .= $base[$result];
            } elseif ($result > 1000) {
                $roman .= $this->numberToRoman($result, 1000, 'M');
            } elseif ($result > 500 && $result < 900) {
                $roman .= $base[500];
                $roman .= $this->numberToRoman($result - 500, 100, 'C');
            } elseif ($result > 100 && $result < 400) {
                $roman .= $this->numberToRoman($result, 100, 'C');
            } elseif ($result > 50 && $result < 90) {
                $roman .= $base[50];
                $roman .= $this->numberToRoman($result - 50, 10, 'X');
            } elseif ($result > 10 && $result < 40) {
                $roman .= $this->numberToRoman($result, 10, 'X');
            } elseif ($result > 5 && $result < 9) {
                $roman .= $base[5];
                $roman .= $this->numberToRoman($result - 5, 1, 'I');
            } else {
                $roman .= $this->numberToRoman($result, 1, 'I');
            }
        }
        return $roman;
    }

    protected function numberToRoman($number, $decimalDegree, $romanLetter)
    {
        return implode('', array_fill(0, $number / $decimalDegree, $romanLetter));
    }
}



$nums = [
    [
        'input' => 111,
        'output' => "CXI"
    ],
    [
        'input' => 3749,
        'output' => "MMMDCCXLIX"
    ],
    [
        'input' => 58,
        'output' => "LVIII"
    ],
    [
        'input' => 1994,
        'output' => "MCMXCIV"
    ]
];

foreach ($nums as $num) {
    $result = (new Solution)->intToRoman($num['input']);
    if ($result == $num['output']) {
        echo "Success input: " . $num['input'] . " output: $result \n";
    } else {
        echo "Error input: " . $num['input'] . " output: $result. Output must be " . $num['output'] . " \n";
    }
}
