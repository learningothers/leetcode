<?php


class Solution
{

    /**
     * @param String $haystack
     * @param String $needle
     * @return Integer
     */
    function strStr($haystack, $needle)
    {
        $lenh = strlen($haystack);
        $lenn = strlen($needle);
        if ($lenn > $lenh) {
            return -1;
        }
        for ($i = 0; $i < $lenh; $i++) {
            if ($haystack[$i] == $needle[0]) {
                $ok = true;
                $index = $i;
                $i++;
                for ($j = 1; $j < $lenn; $j++) {
                    if ($haystack[$i] != $needle[$j]) {
                        $ok = false;
                        break;
                    }
                    $i++;
                }
                $i = $index;
                if ($ok) {
                    return $index;
                }
            }
        }
        return -1;
    }
}


$sol = new Solution;

echo $sol->strStr('leetcode', 'leet') . PHP_EOL;
