<?php


class Solution
{
    function trap1($height)
    {
        $len = count($height);
        $max = $this->max($height);
        echo   $height[$max] . "\n";
        $capacity = 0;
        $lcol = 0;
        $rcol = 0;
        while (true) {
            $temp = 0;

            if ($lcol == $rcol) {
                $rcol++;
            }

            if ($rcol >= $len) {
                break;
            }


            while ($height[$lcol] > $height[$rcol]) {
                // echo "lcol=$lcol, rcol=$rcol, len=$len \n";
                // sleep(1);
                $temp += $height[$lcol] - $height[$rcol];

                $rcol++;

                if ($rcol >= $len) {
                    break;
                }
            }



            $lcol = $rcol;


            // echo "lcol=$lcol, rcol=$rcol, len=$len \n";
            echo "temp = $temp, capacity = $capacity\n";
            $capacity += $temp;
        }
        return $capacity;
    }


    function trap($height)
    {
        $len = count($height);
        $max = 0;
        $min = 0;
        $capacity = 0;
        $temp = 0;
        for ($i = 1; $i < $len; $i++) {

            $current = $height[$i];
            
            if ($current < $height[$max]) {

                $temp += $height[$max] - $current;

                if ($current > $min) {
                    $min = $i;
                }
                if ($current > $height[$i - 1]) {
                    $diff = $height[$max] - $current;
                    $sub = $diff * ($i - $max);
                    $capacity += $temp - $sub;
                    if ($height[$min] > $current) {
                        $capacity += $height[$min] - $current;
                    }
                }
            } else {
                $plus = ($height[$max] - $height[$min]) * ($i - $max);
                $sub = ($i - $min);
                $capacity += $sub + $plus;
                $max = $i;
                $temp = 0; 
            }
            
        }
        return $capacity;
    }
}

echo (new Solution)->trap([4, 2, 0, 3, 2, 5]) . PHP_EOL;
echo (new Solution)->trap([4, 2, 0, 3, 2, 5]) . PHP_EOL;
