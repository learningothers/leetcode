<?php

class Solution
{

    public function find(&$ans, $s, $n, $o, $c)
    {
        if ($o > $n || $c > $o ||  strlen($s) > 2 * $n) return;
        if ($o == $n && $c == $n && strlen($s) == 2 * $n) {
            $ans[] = $s;
        }
        $this->find($ans, $s . "(", $n, $o + 1, $c);
        $this->find($ans, $s . ")", $n, $o, $c + 1);
    }
    public function generateParenthesis($n)
    {
        $ans = [];
        $this->find($ans, "", $n, 0, 0);
        return $ans;
    }
}


echo json_encode((new Solution)->generateParenthesis(1)) . PHP_EOL;
