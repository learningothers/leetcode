<?php


function removeElement(&$nums, $val)
{
    $k = 0;
    foreach ($nums as $i => $element) {
        if ($element != $val) {
            $nums[$k] = $nums[$i];
            $k++;
        }
    }
    return $k;
}
