<?php

class Solution
{

    function isMatch($s, $p)
    {
        $lenp = strlen($p);
        $lens = strlen($s);
        return $this->detect($s, $p, $lenp, $lens);
    }

    function detect($s, $p, $lenp, $lens, $minp = 0, $mins = 0)
    {
        $i = $minp;
        $j = $mins;
        $bool = true;
        while ($i < $lenp) {
            if ($p[$i] == '?') {
                // echo "i=$i, j=$j  \n";
                if (isset($s[$j])) {
                    $i++;
                    $j++;
                    $bool = true;
                    continue;
                } else {
                    return false;
                }
            } else if ($p[$i] == '*') {

                if (!isset($p[$i + 1]) ) {
                    return true;
                }
                $next = $p[$i + 1];
                if ($lens == 0) {
                    return $this->detect($s, $p, $lenp, $lens, $i + 1, 0);
                }
                if ($next == '*') {
                    // echo "aaaa \n";
                    return $this->detect($s, $p, $lenp, $lens, $i + 1, $j);
                } else if ($next == '?') {
                    $difp = $lenp - $i;
                    $difs = $lens - $j;
                    if ($difp > $difs) {
                        echo "i=$i,disp=$difp, j=$j,difs=$difs \n";
                        return $this->detect($s, $p, $lenp, $lens, $i + 1, $j);
                    } else if ($difp == $difs) {
                        return true;
                    }
                } else {
                    for ($m = $i; $m < $lens; $m++) {
                        if ($s[$m] == $next) {
                            $isMatch = $this->detect($s, $p, $lenp, $lens, $i + 1, $m);
                            if ($isMatch) {
                                return true;
                            }
                        }
                    }
                }

                $bool = false;
                $i++;
                continue;
            } else if (isset($s[$j]) && ($p[$i] == $s[$j])) {
                $i++;
                $j++;
                $bool = true;
                continue;
            } else {
                return false;
            }
            $i++;
            $j++;
        }
        if (isset($s[$j])) {
            $bool = false;
        }
        return $bool;
    }
}

// $s = "caaaaaaaa";
// $p = "*?*";

$s = "awwwwwwwwwwwwwa";
$p = "*?a*";

$sol = new Solution;
echo $sol->isMatch($s, $p) . PHP_EOL;
