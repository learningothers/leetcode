<?php



class Solution
{

    /**
     * @param String[] $strs
     * @return String
     */
    function longestCommonPrefix($strs)
    {
        $lastindex = 0;
        $ok = false;
        $len = count($strs);
        if ($len == 1) {
            return $strs[0];
        } elseif ($len == 0) {
            return "";
        }


        $str = $strs[0];
        $firstlen = strlen($str);
        $s = 0;
        $e = $firstlen - 1;
        while ($e >= 0) {
            // echo "start=$s, end=$e, str=". substr($str, $s, $e - $s + 1) ." \n";
            $bool = true;
            for ($j = 1; $j < $len; $j++) {
                $min = $s;
                $max = $e;
                $current = $strs[$j];
                $match = true;
                while ($min <= $max) {
                    // echo "current=$current, minstr=".$str[$min].",maxstr=".$str[$max].", mincurrent=".$current[$min].",maxcurrent=".$current[$max]." \n";
                    if (!isset($current[$min]) || !isset($current[$max])) {
                        $match = false;
                        break;
                    }
                    if (($str[$min] == $current[$min]) && ($str[$max] == $current[$max])) {
                        $min++;
                        $max--;
                        $match = true;
                    } else {
                        $match = false;
                        break;
                    }
                }
                if (!$match) {
                    $bool = false;
                    break;
                }
            }
            if ($bool) {
                $ok = true;
                if ($e > $lastindex) {
                    $lastindex = $e;
                }
            }
            $e--;
        }
        if ($ok) {
            return substr($str, $s, $lastindex - $s + 1);
        } else {
            return "";
        }
    }
}

$sol = new Solution;
$data = ["dog","racecar","car"];
// var_dump($sol->match('flower', 0, 4, 'flowa'));
echo $sol->longestCommonPrefix($data) . PHP_EOL;
