<?php

class Solution
{

    
    function isPalindrome($x)
    {
        if ($x < 0) {
            return false;
        } else if ($x == 0) {
            return true;
        } else {
            $c = $x;
            $max = 0;
            while ($c > 10) {
                $c /= 10;
                $max++;
            }
            while ($x > 0) {
                $pow = pow(10, $max);
                $qoldiq = $x % $pow;
                $start = ($x - $qoldiq) / $pow;
                $end = $x % 10;
                // echo "max = $max, x=$x, qoldiq=$qoldiq, start=$start, end=$end\n";
                if ($start == $end) {
                    $x = ($qoldiq - $end) / 10;
                    $max -= 2;
                } else {
                    return false;
                }
            }
            return true;
        }
    }
}


$sol = new Solution;

echo json_encode(['a' => $sol->isPalindrome(323)]) . PHP_EOL;
