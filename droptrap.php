<?php


class Solution
{

    function trap($height)
    {
        $len = count($height);
        $max = $this->max($height);
        $left = $this->trapLeft($height, $len, $max);
        $right = $this->trapRight($height, $len, $max);
        return $right + $left;
    }

    public function trapLeft($height, $len, $max)
    {
        $capacity = 0;
        $lcol = 0;
        $rcol = 0;
        while (true) {
            $temp = 0;
            if ($lcol == $rcol) {
                $rcol++;
            }
            if ($rcol >= $max) {
                break;
            }
            while ($height[$lcol] > $height[$rcol]) {
                $temp += $height[$lcol] - $height[$rcol];
                $rcol++;

                if ($rcol >= $max) {
                    break;
                }
            }
            $lcol = $rcol;
            $capacity += $temp;
        }
        return $capacity;
    }

    public function trapRight($height, $len, $max)
    {
        $capacity = 0;
        $lcol = $len - 1;
        $rcol = $len - 1;
        while (true) {
            $temp = 0;
            if ($lcol == $rcol) {
                $lcol--;
            }
            if ($lcol <= $max) {
                break;
            }
            while ($height[$lcol] < $height[$rcol]) {
                $temp += $height[$rcol] - $height[$lcol];
                $lcol--;

                if ($lcol <= $max) {
                    break;
                }
            }
            $rcol = $lcol;
            $capacity += $temp;
        }
        return $capacity;
    }


    public function max($array)
    {
        $index = 0;
        $max = $array[$index];
        $len = count($array);
        for ($i = 1; $i < $len; $i++) {
            if ($array[$i] > $max) {
                $max = $array[$i];
                $index = $i;
            }
        }
        return $index;
    }
}

echo (new Solution)->trap([0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]) . PHP_EOL;
echo (new Solution)->trap([4, 2, 0, 3, 2, 5]) . PHP_EOL;
