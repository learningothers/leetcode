<?php


function searchRange($nums, $target)
{
    $len = count($nums);
    $min = 0;
    $max = $len - 1;
    while ($min <= $max) {
        $mid = floor($min + ($max - $min) / 2);
        if ($nums[$mid] > $target) {
            $max = $mid - 1;
        } else if ($nums[$mid] < $target) {
            $min = $mid + 1;
        } else {
            $minl = $min;
            $maxl = $mid;
            while ($minl <= $maxl) {
                $midl = floor($minl + ($maxl - $minl) / 2);
                if ($nums[$midl] == $target) {
                    $maxl = $midl - 1;
                } else if ($nums[$midl] < $target) {
                    $minl = $midl + 1;
                }
            }

            $minr = $mid;
            $maxr = $max;
            while ($minr <= $maxr) {
                $midr = floor($minr + ($maxr - $minr) / 2);
                if ($nums[$midr] == $target) {
                    $minr = $midr + 1;
                } else if ($nums[$midr] > $target) {
                    $maxr = $midr - 1;
                }
            }
            return [$minl, $maxr];
        }
    }
    return [-1, -1];
}

$nums = [];
$target = 0;
echo json_encode(searchRange($nums, $target)) . PHP_EOL;
// echo $nums[15] . PHP_EOL;
