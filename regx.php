<?php


class Solution
{
    public function isMatch($s, $p)
    {
        return $this->matcher($s, $p);
    }

    public function matcher($s, $p, $min = null, $max = null)
    {
        $len = count($p);
        for ($i = 0; $i < $len; $i++) {
            if ($p[$i] == '*') {

            } else if ($p[$i] == '.') {

            } else if ($p[$i] != $p[$i]) {
                return $this->matcher($s, $p, $i+1, $len);
            }
        }
    }
}


$s = 'aab';
$p = 'c*a*b';
echo json_encode((new Solution)->isMatch($s, $p)) . PHP_EOL;
