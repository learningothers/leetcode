<?php

class ListNode
{
    public $val = 0;
    public ?ListNode $next = null;
    function __construct($val = 0, $next = null)
    {
        $this->val = $val;
        $this->next = $next;
    }
}


class Solution
{
    function mergeKLists($lists)
    {
        $len = count($lists);
        if ($len == 0) return null;
        
        $left = null;
        $right = $lists[0];

        for ($i = 1; $i < $len; $i++) {
            $klist = $lists[$i];
            while ($klist) {
                if (!$right) {
                    $right = $klist;
                    break;
                }
                if ($right->val <= $klist->val) {
                    $next = $right->next;
                    $right->next = $left;
                    $left = $right;
                    $right = $next;
                    continue;
                }
                $klistnext = $klist->next; 
                $klist->next = $right;
                $right = $klist;
                $klist = $klistnext;
            }
            while ($left) {
                $next = $left->next;
                $left->next = $right;
                $right = $left;
                $left = $next;
            }
        }
        return $right;
    }
    /**
     * @param ListNode $list1
     * @param ListNode $list2
     * @return ListNode
     */
    function mergeTwoLists($list1, $list2)
    {
        $left = null;
        $right = $list1;
        while ($list2) {
            if (!$right) {
                $right = $list2;
                break;
            }
            if ($right->val <= $list2->val) {
                $next = $right->next;
                $right->next = $left;
                $left = $right;
                $right = $next;
                continue;
            }
            $list2next = $list2->next;
            $list2->next = $right;
            $right = $list2;
            $list2 = $list2next;
        }
        while ($left) {
            $next = $left->next;
            $left->next = $right;
            $right = $left;
            $left = $next;
        }

        return $right;
    }

    public function toListNode($array)
    {
        // try {
        //     echo 'left=' . json_encode($this->toArray($left)) . PHP_EOL;
        //     echo 'right=' . json_encode($this->toArray($right)) . PHP_EOL;
        //     echo 'list2=' . json_encode($this->toArray($list2)) . PHP_EOL . PHP_EOL . PHP_EOL;

        // } catch (\Throwable $th) {
        //     // var_dump($right);
        //     // var_dump($list2);
        //     // var_dump($list2next);
        //     die;
        // }
        $len = count($array);
        $listNode = null;
        while ($len > 0) {
            $listNode = new ListNode(val: $array[$len - 1], next: $listNode);
            $len--;
        }
        return $listNode;
    }

    public function toArray($listNode)
    {
        if (!$listNode) return [];
        $array = [$listNode->val];
        while ($next = $listNode->next) {
            $array[] = $next->val;
            $listNode = $next;
        }
        return $array;
    }
}


$sol = new Solution;
$list1 = [1, 4, 5];
$list2 = [1, 3, 4];
$list3 = [2, 6];

$head1 = $sol->toListNode($list1);
$head2 = $sol->toListNode($list2);
$head3 = $sol->toListNode($list3);

$lists = [$head1, $head2, $head3];

$head = $sol->mergeKLists($lists);

echo json_encode($sol->toArray($head)) . PHP_EOL;
