<?php



class ListNode
{
    public $val = 0;
    public ?ListNode $next = null;
    function __construct($val = 0, $next = null)
    {
        $this->val = $val;
        $this->next = $next;
    }
}

class Solution
{

    /**
     * @param ListNode $head
     * @param Integer $n
     * @return ListNode
     */
    function removeNthFromEnd($head, $n)
    {
        return $this->rm($head, $n);
    }

    public function rm($listNode, $n)
    {

        $listNode = $this->reverse($listNode);
        if (! $listNode) return;
        $node = null;
        $step = 1;
        while ($next = $listNode->next) {
            if ($step != $n) {
                $listNode->next = $node;
                $node = $listNode;
            }
            $listNode = $next;
            $step++;
        }
        if ($step == $n) {
            $listNode = $node;
        } else {
            $listNode->next = $node;
        }
        return $listNode;
    }


    public function toListNodeRecursive($array, $index = 0)
    {
        if (isset($array[$index])) {
            return new ListNode(val: $array[$index], next: $this->toListNodeRecursive($array, $index + 1));
        }
    }

    public function toListNode($array)
    {
        $len = count($array);
        $listNode = null;
        while ($len > 0) {
            $listNode = new ListNode(val: $array[$len - 1], next: $listNode);
            $len--;
        }
        return $listNode;
    }


    public function reverseRecursive($listNode, $nextNode = null)
    {
        if (! $listNode) return;
        if ($node = $listNode->next) {
            $listNode->next = $nextNode;
            return $this->reverseRecursive($node, $listNode);
        }
        $listNode->next = $nextNode;
        return $listNode;
    }

    public function reverse($listNode)
    {
        if (! $listNode) return;
        $node = null;
        while ($next = $listNode->next) {
            $listNode->next = $node;
            $node = $listNode;
            $listNode = $next;
        }
        $listNode->next = $node;
        return $listNode;
    }

    public function toArrayRecursive($listNode)
    {
        if (! $listNode) return [];
        $array = [$listNode->val];
        if ($listNode->next) {
            $array = [...$array, ...$this->toArrayRecursive($listNode->next)];
        }
        return $array;
    }

    public function toArray($listNode)
    {
        if (! $listNode) return [];
        $array = [$listNode->val];
        while ($next = $listNode->next) {
            $array[] = $next->val;
            $listNode = $next;
        }
        return $array;
    }
}

$sol = new Solution;
$listNode = $sol->toListNode([1]);
$listNode = $sol->rm($listNode, 1);
$array = $sol->toArray($listNode);
echo json_encode($array) . PHP_EOL;

$listNodeReversed = $sol->reverse($listNode);
$array = $sol->toArray($listNodeReversed);
echo json_encode($array) . PHP_EOL;



// var_dump($listNode);