<?php


class Solution
{

    /**
     * @param Integer[] $nums
     * @param Integer $target
     * @return Integer
     */
    function search($nums, $target)
    {
        $len = count($nums);


        // $mx = $len - 1;
        // $mn = 0;
        // while ($mx - $mn != 1) {
        //     sleep(1);
        //     echo "min=$mn, max=$mx \n";
        //     $md = round(($mn + $mx) / 2);
        //     if ($nums[$md] > $nums[$mn]) {
        //         $mn = $md;
        //     } else if ($nums[$md] < $nums[$mn]) {
        //         $mx = $md;
        //     } else {
        //         var_dump("asasa");die;
        //     }
        // }
        $min = 0;
        $max = $len - 1;

        if ($min == $max && $nums[$min] == $target) {
            return $min;
        }

        while ($max > $min) {
            if ($min == $len - 2 && $nums[$max] == $target) {
                return $max;
            }
            $mid = round(($min + $max) / 2, 0, PHP_ROUND_HALF_DOWN);
            if ($nums[$mid] > $nums[$min]) {
                if ($nums[$min] > $target) {
                    $min = $mid;
                } else if ($nums[$min] < $target) {
                    if ($nums[$mid] > $target) {
                        $max = $mid;
                    } else if ($nums[$mid] < $target) {
                        $min = $mid;
                    } else {
                        return $mid;
                    }
                } else {
                    return $min;
                }
            } else {
                if ($nums[$min] < $target) {
                    $max = $mid;
                } else if ($nums[$min] > $target) {
                    if ($nums[$mid] > $target) {
                        $max = $mid;
                    } else if ($nums[$mid] < $target) {
                        $min = $mid;
                    } else {
                        return $mid;
                    }
                } else {
                    return $min;
                }
            }
        }
        return -1;
    }

    function dd(...$args)
    {
        echo implode(',', $args) . PHP_EOL;
        usleep(150000);
    }
}

$sol = new Solution;
echo $sol->search([5, 1, 3], 5) . PHP_EOL;
                //[0, 1, 2, 3, 4, 5 ,6 , 7 , 8 , 9 , 10, 11, 12,13,14,15]