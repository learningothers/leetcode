<?php


class Solution
{
    public $keys = [];

    function isValid($s)
    {
        $len = strlen($s);
        if ($len % 2 > 0) {
            return false;
        }
        $stack = [];
        for ($i = 0; $i < $len; $i++) {
            if ($s[$i] == '(' || $s[$i] == '[' || $s[$i] == '{') {
                $stack[] = $s[$i];
            } else {
                $last = end($stack);
                $current = ($s[$i] == ')') ? '(' : ($s[$i] == ']' ? '[' : '{');
                if ($last == $current) {
                    array_pop($stack);
                } else {
                    return false;
                }
            }
        }
        return empty($stack);
    }
}

$sol = new Solution;

$data = '(]';
// $ar = [];
// var_dump(['a' => end($ar)]);
echo json_encode(['ok' => $sol->isValid($data)]) . PHP_EOL;
