<?php


class ListNode
{
    public $val = 0;
    public ?ListNode $next = null;
    function __construct($val = 0, $next = null)
    {
        $this->val = $val;
        $this->next = $next;
    }
}

class Solution
{
    /**
     * @param ListNode $head
     * @param int $k
     * @return ListNode
     */
    function reverseKGroup($head, $k)
    {
        if (!$head?->next) {
            return $head;
        } else {
            $s = $k;
            $temp = clone $head;
            $prev = null;
            while ($s > 0) {
                if (!$temp) {
                    $prev = null;
                    break;
                }
                $current = clone $temp;
                $current->next = $prev;
                $prev = $current;
                $temp = $temp?->next;
                $s--;
            }
            if ($prev) {
                $next = $this->reverseKGroup($temp, $k);
                return $this->setNext($prev, $next);
            } else {
                return $head;
            }
        }
    }
    /**
     * @param ListNode $head
     * @param ListNode $next
     * @return ListNode
     */
    public function setNext($head, $next)
    {
        if ($head->next) {
            $head->next = $this->setNext($head->next, $next);
        } else {
            $head->next = $next;
        }
        return $head;
    }

    public function toListNode($array)
    {
        $len = count($array);
        $listNode = null;
        while ($len > 0) {
            $listNode = new ListNode(val: $array[$len - 1], next: $listNode);
            $len--;
        }
        return $listNode;
    }

    public function toArray($listNode)
    {
        if (!$listNode) return [];
        $array = [$listNode->val];
        while ($next = $listNode->next) {
            $array[] = $next->val;
            $listNode = $next;
        }
        return $array;
    }
}

$sol = new Solution;

$array = [1, 2, 3, 4, 5];
$l = $sol->toListNode($array);
$l = $sol->reverseKGroup($l, 2);
$array = $sol->toArray($l);
echo json_encode($array) . PHP_EOL;
