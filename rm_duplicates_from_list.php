<?php


function removeDuplicates(&$nums)
{
    $k = 1;
    $current = $nums[0];
    $len = count($nums);
    for ($i = 1; $i < $len; $i++) {
        if ($nums[$i] == $current) {
            unset($nums[$i]);
        } else {
            $k++;
            $current = $nums[$i];
        }
    }
    return $k;
}
